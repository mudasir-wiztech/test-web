importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

firebase.initializeApp({
  apiKey: "AIzaSyCG0iBzb9z0ngoDVge2SUQKiP4lQOe1PiI",
  authDomain: "hometrumpeter.firebaseapp.com",
  projectId: "hometrumpeter",
  storageBucket: "hometrumpeter.appspot.com",
  messagingSenderId: "552317406916",
  appId: "1:552317406916:web:5b797fe6cdad6e75e50391",
  measurementId: "G-5QT5JQZWNY"
});

const messaging = firebase.messaging();

// Optional:
messaging.onBackgroundMessage((message) => {
  console.log("onBackgroundMessage", message);
});